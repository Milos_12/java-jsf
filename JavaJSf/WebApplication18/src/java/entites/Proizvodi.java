/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MILOS
 */
@Entity
@Table(name = "proizvodi")
@NamedQueries({
    @NamedQuery(name = "Proizvodi.findAll", query = "SELECT p FROM Proizvodi p")
    , @NamedQuery(name = "Proizvodi.findByIdProizvodi", query = "SELECT p FROM Proizvodi p WHERE p.idProizvodi = :idProizvodi")
    , @NamedQuery(name = "Proizvodi.findByNaziv", query = "SELECT p FROM Proizvodi p WHERE p.naziv = :naziv")
    , @NamedQuery(name = "Proizvodi.findByKolicina", query = "SELECT p FROM Proizvodi p WHERE p.kolicina = :kolicina")
    , @NamedQuery(name = "Proizvodi.findByCena", query = "SELECT p FROM Proizvodi p WHERE p.cena = :cena")})
public class Proizvodi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proizvodi")
    private Integer idProizvodi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "naziv")
    private String naziv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "kolicina")
    private String kolicina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cena")
    private int cena;

    public Proizvodi() {
    }

    public Proizvodi(Integer idProizvodi) {
        this.idProizvodi = idProizvodi;
    }

    public Proizvodi(Integer idProizvodi, String naziv, String kolicina, int cena) {
        this.idProizvodi = idProizvodi;
        this.naziv = naziv;
        this.kolicina = kolicina;
        this.cena = cena;
    }

    public Integer getIdProizvodi() {
        return idProizvodi;
    }

    public void setIdProizvodi(Integer idProizvodi) {
        this.idProizvodi = idProizvodi;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getKolicina() {
        return kolicina;
    }

    public void setKolicina(String kolicina) {
        this.kolicina = kolicina;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProizvodi != null ? idProizvodi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proizvodi)) {
            return false;
        }
        Proizvodi other = (Proizvodi) object;
        if ((this.idProizvodi == null && other.idProizvodi != null) || (this.idProizvodi != null && !this.idProizvodi.equals(other.idProizvodi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.Proizvodi[ idProizvodi=" + idProizvodi + " ]";
    }
    
}
