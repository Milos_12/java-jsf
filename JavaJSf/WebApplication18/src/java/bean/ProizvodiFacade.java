/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entites.Proizvodi;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MILOS
 */
@Stateless
public class ProizvodiFacade extends AbstractFacade<Proizvodi> {

    @PersistenceContext(unitName = "WebApplication18PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProizvodiFacade() {
        super(Proizvodi.class);
    }
    
}
