/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entites.Kupci;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MILOS
 */
@Stateless
public class KupciFacade extends AbstractFacade<Kupci> {

    @PersistenceContext(unitName = "WebApplication18PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KupciFacade() {
        super(Kupci.class);
    }
    
}
